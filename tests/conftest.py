# _*_ coding: utf-8 _*_
import os

FIXTURE_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'testdata')
