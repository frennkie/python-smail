BobRSASignByCarl_password.txt

The file BobRSASignByCarl_password.p12 contains the following data:
1) Private RSA key for "Bob"
2) A signed X.509 certificate for "Bob" for the same private key
3) The public certificate for "Carl" which acts as a CA and which signed the "Bob" certificate.

The password/passphrase for the .p12 file is: "password" (without the quotes).